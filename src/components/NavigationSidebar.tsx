import { LucideGavel, LucideKeyRound, LucideWebhook } from 'lucide-react';
import Link from 'next/link';
import { Button } from './ui/button';
import Profile from './Profile';

export default function NavigationSidebar() {
	return (
		<div className="relative min-w-[300px] w-1/12 min-h-screen dark:border-r p-8 ">
			<h1 className="text-center text-xl font-medium">
				Suisei&apos;s Mic
				<br />
				{' '}
				Developer portal
			</h1>

			<div className="mt-6">
				<div className="flex flex-col gap-2">
					<h2 className="flex text-lg font-medium">
						<LucideGavel className="mr-2" />
						Bans API
					</h2>
					<div className="ml-6">
						<Button variant="link">
							<Link href="/bans/keys" className="flex hover:bg-">
								<LucideKeyRound className="mr-2" />
								API Keys
							</Link>
						</Button>
						<Button variant="link">
							<Link href="/bans/webhooks" className="flex hover:bg-">
								<LucideWebhook className="mr-2" />
								Webhooks
							</Link>
						</Button>
					</div>
				</div>
			</div>

			<div className="absolute bottom-8 left-8 min-w-[240px] w-1/12">
				<Profile />
			</div>
		</div>
	);
}
