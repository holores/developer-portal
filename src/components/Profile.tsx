'use client';

import { useSession } from 'next-auth/react';
import Link from 'next/link';
import { LucideLogOut } from 'lucide-react';
import { Skeleton } from './ui/skeleton';
import { Avatar, AvatarFallback, AvatarImage } from './ui/avatar';

export default function Profile() {
	const { data: session, status } = useSession();

	if (status !== 'authenticated') {
		return (
			<div className="flex items-center gap-4">
				<Skeleton className="h-12 w-12 rounded-full" />
				<Skeleton className="h-4 w-20" />
			</div>
		);
	}

	return (
		<div className="flex justify-between items-center">
			<Link
				href="/profile"
				className="flex items-center gap-4 hover:underline"
			>
				<Avatar>
					<AvatarImage src={session?.user?.image ?? ''} alt={session?.user?.name ?? ''} />
					<AvatarFallback>PFP</AvatarFallback>
				</Avatar>
				<p>{session?.user?.name}</p>
			</Link>

			<Link href="/api/auth/signout" className="flex">
				<LucideLogOut />
			</Link>
		</div>
	);
}
