'use client';

import { SessionProvider as NextAuthSessionProvider } from 'next-auth/react';

interface IProps {
	children: React.ReactNode;
}

export default function SessionProvider({ children }: IProps) {
	return (
		<NextAuthSessionProvider>
			{children}
		</NextAuthSessionProvider>
	);
}
