'use client';

import { useEffect, useState } from 'react';
import { useTheme } from 'next-themes';
import { LucideMoon, LucideSun } from 'lucide-react';
import { Skeleton } from './ui/skeleton';
import { Button } from './ui/button';

export default function ThemeSwitcher() {
	const [mounted, setMounted] = useState(false);
	const { resolvedTheme, setTheme } = useTheme();

	// useEffect only runs on the client, so now we can safely show the UI
	useEffect(() => {
		setMounted(true);
	}, []);

	if (!mounted) {
		return (
			<Skeleton className="w-12 h-12 rounded-full absolute bottom-8 right-8" />
		);
	}

	return (
		<Button
			variant="outline"
			className="w-12 h-12 rounded-full absolute bottom-8 right-8 grid place-items-center border-2 border-accent cursor-pointer"
			onClick={() => setTheme(resolvedTheme === 'light' ? 'dark' : 'light')}
		>
			{resolvedTheme === 'light' ? <LucideMoon className="w-6 h-6" /> : <LucideSun className="w-6 h-6" /> }
		</Button>
	);
}
