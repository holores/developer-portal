/* eslint-disable import/prefer-default-export */
import { getServerSession } from 'next-auth';
import authOptions from 'lib/authOptions';
import { NextResponse } from 'next/server';
import * as process from 'process';

export async function POST(request: Request, { params }: { params: { id: string } }) {
	const session = await getServerSession(authOptions);

	if (!session) {
		return new NextResponse(null, {
			status: 401,
		});
	}

	return fetch(`${process.env.BANS_API_URL as string}/admin/apikey/${params.id}/rollover`, {
		method: 'POST',
		headers: {
			'X-API-KEY': process.env.BANS_API_TOKEN as string,
		},
	});
}
