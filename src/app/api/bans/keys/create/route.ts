/* eslint-disable import/prefer-default-export */
import { getServerSession } from 'next-auth';
import authOptions from 'lib/authOptions';
import { NextResponse } from 'next/server';
import * as process from 'process';
import { getToken } from 'next-auth/jwt';

export async function POST(request: Request) {
	const session = await getServerSession(authOptions);

	if (!session) {
		return new NextResponse(null, {
			status: 401,
		});
	}

	const token = await getToken({ req: request as any });

	return fetch(`${process.env.BANS_API_URL as string}/admin/apikey/create`, {
		method: 'POST',
		headers: {
			'X-API-KEY': process.env.BANS_API_TOKEN as string,
			'Content-Type': 'application/json',
		},
		body: JSON.stringify({
			...(await request.json()),
			owner: token!.id,
		}),
	});
}
