/* eslint-disable import/prefer-default-export */
import { getServerSession } from 'next-auth';
import authOptions from 'lib/authOptions';
import { NextResponse } from 'next/server';
import * as process from 'process';
import { getToken } from 'next-auth/jwt';

export async function GET(request: Request) {
	const session = await getServerSession(authOptions);

	if (!session) {
		return new NextResponse(null, {
			status: 401,
		});
	}

	const token = await getToken({ req: request as any });

	const res = await fetch(`${process.env.BANS_API_URL as string}/admin/webhook/list?owner=${token!.id}`, {
		headers: {
			'X-API-KEY': process.env.BANS_API_TOKEN as string,
		},
	});

	return NextResponse.json(await res.json());
}
