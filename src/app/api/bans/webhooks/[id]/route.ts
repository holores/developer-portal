import { getServerSession } from 'next-auth';
import authOptions from 'lib/authOptions';
import { NextResponse } from 'next/server';
import * as process from 'process';

export async function DELETE(request: Request, { params }: { params: { id: string } }) {
	const session = await getServerSession(authOptions);

	if (!session) {
		return new NextResponse(null, {
			status: 401,
		});
	}

	return fetch(`${process.env.BANS_API_URL as string}/admin/webhook/${params.id}`, {
		method: 'DELETE',
		headers: {
			'X-API-KEY': process.env.BANS_API_TOKEN as string,
		},
	});
}

export async function PATCH(request: Request, { params }: { params: { id: string } }) {
	const session = await getServerSession(authOptions);

	if (!session) {
		return new NextResponse(null, {
			status: 401,
		});
	}

	return fetch(`${process.env.BANS_API_URL as string}/admin/webhook/${params.id}`, {
		method: 'PATCH',
		headers: {
			'X-API-KEY': process.env.BANS_API_TOKEN as string,
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(await request.json()),
	});
}
