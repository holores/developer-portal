import NavigationSidebar from 'components/NavigationSidebar';
import SessionProvider from 'components/providers/SessionProvider';

interface IProps {
	children: React.ReactNode;
}

export default function RootLayout({ children }: IProps) {
	return (
		<SessionProvider>
			<div className="flex">
				<NavigationSidebar />
				{children}
			</div>
		</SessionProvider>
	);
}
