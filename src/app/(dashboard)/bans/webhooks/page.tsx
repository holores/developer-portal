'use client';

import { useEffect, useState } from 'react';
import { Skeleton } from 'components/ui/skeleton';
import DataTable from 'components/DataTable';
import {
	DropdownMenu,
	DropdownMenuContent,
	DropdownMenuItem,
	DropdownMenuLabel,
	DropdownMenuSeparator,
	DropdownMenuTrigger,
} from 'components/ui/dropdown-menu';
import { Button } from 'components/ui/button';
import { MoreHorizontal } from 'lucide-react';
import { CellContext } from '@tanstack/react-table';
import { Webhook } from 'lib/BansAPI';
import { Sheet, SheetContent, SheetTitle } from 'components/ui/sheet';
import {
	AlertDialog,
	AlertDialogAction,
	AlertDialogCancel,
	AlertDialogContent,
	AlertDialogDescription,
	AlertDialogFooter,
	AlertDialogHeader,
	AlertDialogTitle,
} from 'components/ui/alert-dialog';
import { Input } from 'components/ui/input';
import { Label } from 'components/ui/label';
import Select from 'react-select';
import { Checkbox } from 'components/ui/checkbox';

type AlertData = {
	type: 'create';
} | {
	type: 'delete' | 'rollover';
	id: string;
} | {
	type: 'secret';
	secret: string;
};

interface WebhookEditState {
	name: string;
	url: string;
	disabled: boolean;
	subscriptions: Array<'USER' | 'USERBANLIST' | 'CONTENT'>;
	id?: string;
}

const subscriptionOptions = [
	{ value: 'USER', label: 'Users' },
	{ value: 'USERBANLIST', label: 'User banlists' },
	{ value: 'CONTENT', label: 'Content' },
];

function WebhookActionsCell(
	setAlertData: (data: AlertData) => void,
	setEditState: (state: WebhookEditState) => void,
) {
	return function Cell({ row }: CellContext<any, any>) {
		return (
			<DropdownMenu>
				<DropdownMenuTrigger asChild>
					<Button variant="ghost" className="h-8 w-8 p-0">
						<span className="sr-only">Open menu</span>
						<MoreHorizontal className="h-4 w-4" />
					</Button>
				</DropdownMenuTrigger>
				<DropdownMenuContent align="end">
					<DropdownMenuLabel>Actions</DropdownMenuLabel>
					<DropdownMenuItem
						className="cursor-pointer"
						onClick={() => setEditState({
							name: row.original.name,
							url: row.original.url,
							disabled: row.original.disabled,
							subscriptions: row.original.subscriptions,
							id: row.original.id,
						})}
					>
						Edit
					</DropdownMenuItem>
					<DropdownMenuSeparator />
					<DropdownMenuLabel className="text-red-600 dark:text-red-500">Danger zone</DropdownMenuLabel>
					<DropdownMenuItem
						className="cursor-pointer"
						onClick={() => setAlertData({ type: 'rollover', id: row.original.id })}
					>
						Reset webhook secret
					</DropdownMenuItem>
					<DropdownMenuItem
						className="cursor-pointer"
						onClick={() => setAlertData({ type: 'delete', id: row.original.id })}
					>
						Delete webhook
					</DropdownMenuItem>
				</DropdownMenuContent>
			</DropdownMenu>
		);
	};
}

function DisabledCell({ row }: CellContext<any, any>) {
	const value = (row.getValue('disabled') as boolean).toString();

	return value.charAt(0).toUpperCase() + value.slice(1);
}

function SubscriptionsCell({ row }: CellContext<any, any>) {
	const subscriptions = row.getValue('subscriptions') as string[];

	return subscriptions.map((value) => subscriptionOptions.find((item) => item.value === value)!.label).join(', ');
}

export default function BansAPIWebhooksPage() {
	const [data, setData] = useState<Webhook[] | null>(null);
	const [alertData, setAlertData] = useState<AlertData | null>(null);
	const [editState, setEditState] = useState<WebhookEditState | null>(null);
	const [newUrl, setNewUrl] = useState<string | null>(null);
	const [newName, setNewName] = useState<string | null>(null);
	const [newDisabled, setNewDisabled] = useState<boolean | null>(null);
	// eslint-disable-next-line max-len
	const [newSubscriptions, setNewSubscriptions] = useState<ReadonlyArray<{ value: string, label: string }> | null>(null);

	useEffect(() => {
		(async () => {
			const res = await fetch('/api/bans/webhooks/list');
			setData(await res.json());
		})();
	}, []);

	if (!data) {
		return (
			<div className="p-8 grid place-items-center w-full">
				<Skeleton className="w-10/12 h-3/4" />
			</div>
		);
	}

	return (
		<Sheet
			open={!!editState?.id}
			onOpenChange={(open) => {
				if (!open) {
					setEditState(null);
					setNewName(null);
					setNewUrl(null);
					setNewSubscriptions(null);
				}
			}}
		>
			<div className="p-8 grid place-items-center w-full">
				<div className="container w-10/12 h-3/4 mx-auto">
					<h1 className="text-3xl font-medium">Manage your webhooks</h1>
					<br />
					<Button onClick={() => {
						setAlertData({ type: 'create' });
					}}
					>
						Create new webhook
					</Button>
					<DataTable
						columns={[
							{
								accessorKey: 'name',
								header: 'Name',
							},
							{
								accessorKey: 'url',
								header: 'URL',
							},
							{
								accessorKey: 'disabled',
								header: 'Disabled',
								cell: DisabledCell,
							},
							{
								accessorKey: 'failures',
								header: 'Failures',
							},
							{
								accessorKey: 'subscriptions',
								header: 'Subscriptions',
								cell: SubscriptionsCell,
							},
							{
								id: 'actions',
								cell: WebhookActionsCell(setAlertData, setEditState),
							},
						]}
						data={data}
					/>
				</div>
			</div>

			<AlertDialog open={!!alertData}>
				<AlertDialogContent>
					<AlertDialogHeader>
						<AlertDialogTitle>
							{(() => {
								switch (alertData?.type) {
									case 'create':
										return 'Create a new webhook';
									case 'secret':
										return 'Webhook secret';
									default:
										return 'Are you absolutely sure?';
								}
							})()}
						</AlertDialogTitle>

						<AlertDialogDescription>
							{(() => {
								switch (alertData?.type) {
									case 'create':
										return 'Create a new webhook to use for the Bans API.';
									case 'delete':
										return 'This action cannot be undone. This will permanently delete the webhook.';
									case 'rollover':
										return 'This action cannot be undone.';
									case 'secret':
										return 'This secret is only shown once, make sure to save it somewhere secure. You can use this secret to verify webhook requests.';
									default:
										return '';
								}
							})()}
						</AlertDialogDescription>
					</AlertDialogHeader>

					{alertData?.type === 'create' && (
						<div className="grid gap-4 py-4">
							<div className="grid grid-cols-4 items-center gap-4">
								<Label htmlFor="name" className="text-right">
									Name
								</Label>
								<Input
									id="name"
									value={newName ?? ''}
									onChange={(e) => setNewName(e.target.value)}
									className="col-span-3"
								/>
							</div>
							<div className="grid grid-cols-4 items-center gap-4">
								<Label htmlFor="url" className="text-right">
									URL
								</Label>
								<Input
									id="url"
									value={newUrl ?? ''}
									type="url"
									onChange={(e) => setNewUrl(e.target.value)}
									className="col-span-3"
								/>
							</div>
							<div className="grid grid-cols-4 items-center gap-4">
								<Label className="text-right">
									Subscriptions
								</Label>
								<Select
									value={newSubscriptions ?? []}
									onChange={(newValue) => setNewSubscriptions(newValue)}
									isMulti
									options={subscriptionOptions as any}
									placeholder="Select events to subscribe to"
									className="col-span-3"
									unstyled
									classNames={{
										menu: () => 'w-full mt-1 py-1 rounded-md border border-accent',
										option: () => 'bg-background w-full px-2 py-2 hover:bg-accent cursor-pointer',
										control: () => 'px-2 rounded-md border border-input',
										valueContainer: () => 'gap-2',
									}}
								/>
							</div>
						</div>
					)}

					{alertData?.type === 'secret' && (
						<div className="grid gap-4 py-4">
							<div className="grid grid-cols-4 items-center gap-4">
								<Label htmlFor="secret" className="text-right">
									Webhook secret
								</Label>
								<Input
									id="secret"
									value={alertData.secret}
									className="col-span-3"
									readOnly
								/>
							</div>
						</div>
					)}

					<AlertDialogFooter>
						{
							alertData?.type === 'secret'
								? (
									<AlertDialogCancel onClick={() => setAlertData(null)}>Close</AlertDialogCancel>
								) : (
									<>
										<AlertDialogCancel onClick={() => {
											setAlertData(null);
											setEditState(null);
											setNewName(null);
											setNewUrl(null);
											setNewSubscriptions(null);
										}}
										>
											Cancel
										</AlertDialogCancel>
										<AlertDialogAction
											onClick={async () => {
												try {
													if (alertData?.type === 'create') {
														const res = await fetch('/api/bans/webhooks/create', {
															method: 'POST',
															headers: {
																'Content-Type': 'application/json',
															},
															body: JSON.stringify({
																name: newName,
																subscriptions: newSubscriptions!.map((item) => item.value),
																url: newUrl,
															}),
														});

														const body = await res.json();

														setAlertData({
															type: 'secret',
															secret: body.secret,
														});
														setData((oldData) => [
															...oldData!,
															{
																name: newName!,
																url: newUrl!,
																id: body.id,
																subscriptions: newSubscriptions!.map((item) => item.value),
																disabled: false,
																failures: 0,
															},
														]);
													} else if (alertData?.type === 'rollover') {
														const res = await fetch(`/api/bans/webhooks/${alertData.id}/rollover`, {
															method: 'POST',
														});

														const body = await res.json();

														setAlertData({
															type: 'secret',
															secret: body.secret,
														});
													} else if (alertData?.type === 'delete') {
														await fetch(`/api/bans/webhooks/${alertData.id}`, {
															method: 'DELETE',
														});

														setData((oldData) => {
															const index = oldData!.findIndex((wh) => wh.id === alertData.id);
															const copy = [...oldData!];
															copy.splice(index, 1);
															return copy;
														});

														setAlertData(null);
													}
												} catch (err) {
													// TODO: Handle errors better
													// eslint-disable-next-line no-console
													console.log(err);
												}

												setEditState(null);
												setNewName(null);
												setNewUrl(null);
												setNewSubscriptions(null);
											}}
										>
											Continue
										</AlertDialogAction>
									</>
								)
						}
					</AlertDialogFooter>
				</AlertDialogContent>
			</AlertDialog>

			<SheetContent className="sm:max-w-md">
				<SheetTitle>Edit webhook</SheetTitle>
				<div className="grid gap-4 py-4">
					<div className="grid grid-cols-4 items-center gap-4">
						<Label htmlFor="name" className="text-right">
							Name
						</Label>
						<Input
							id="name"
							value={newName ?? editState?.name}
							onChange={(e) => setNewName(e.target.value)}
							className="col-span-3"
						/>
					</div>
					<div className="grid gap-4 py-4">
						<div className="grid grid-cols-4 items-center gap-4">
							<Label htmlFor="url" className="text-right">
								URL
							</Label>
							<Input
								id="url"
								value={newUrl ?? editState?.url}
								onChange={(e) => setNewUrl(e.target.value)}
								className="col-span-3"
							/>
						</div>
					</div>
					<div className="grid gap-4 py-4">
						<div className="grid grid-cols-4 items-center gap-4">
							<Label htmlFor="disabled" className="text-right">
								Disabled
							</Label>
							<Checkbox
								id="disabled"
								checked={newDisabled ?? editState?.disabled}
								onCheckedChange={(checked) => setNewDisabled(checked as boolean)}
							/>
						</div>
					</div>
					<div className="grid grid-cols-4 items-center gap-4">
						<Label className="text-right">
							Subscriptions
						</Label>
						<Select
							value={
								newSubscriptions
								?? editState?.subscriptions.map((value) => subscriptionOptions.find(
									(item) => item.value === value,
								)) as any
							}
							onChange={(newValue) => setNewSubscriptions(newValue)}
							isMulti
							options={subscriptionOptions as any}
							placeholder="Select events to subscribe to"
							className="col-span-3"
							unstyled
							classNames={{
								menu: () => 'w-full mt-1 py-1 rounded-md border border-accent',
								option: () => 'bg-background w-full px-2 py-2 hover:bg-accent cursor-pointer',
								control: () => 'px-2 rounded-md border border-input',
								valueContainer: () => 'gap-2',
							}}
						/>
						<Button onClick={async () => {
							try {
								const res = await fetch(`/api/bans/webhooks/${editState!.id}`, {
									method: 'PATCH',
									headers: {
										'Content-Type': 'application/json',
									},
									body: JSON.stringify({
										name: newName ?? undefined,
										url: newUrl ?? undefined,
										disabled: newDisabled ?? undefined,
										subscriptions: newSubscriptions?.map((item) => item.value) ?? undefined,
									}),
								});

								const body = await res.json();

								setData((oldData) => {
									const index = oldData!.findIndex((wh) => wh.id === editState!.id);
									const copy = [...oldData!];
									copy[index] = {
										id: body.id,
										name: body.name,
										url: body.url,
										disabled: body.disabled,
										failures: body.failures,
										subscriptions: body.subscriptions,
									};

									return copy;
								});
							} catch (err) {
								// TODO: Handle errors better
								// eslint-disable-next-line no-console
								console.error(err);
							}

							setEditState(null);
							setNewName(null);
							setNewUrl(null);
							setNewDisabled(null);
							setNewSubscriptions(null);
						}}
						>
							Save
						</Button>
					</div>
				</div>
			</SheetContent>
		</Sheet>
	);
}
