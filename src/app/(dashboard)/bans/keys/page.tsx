'use client';

import { useEffect, useState } from 'react';
import { Skeleton } from 'components/ui/skeleton';
import DataTable from 'components/DataTable';
import {
	DropdownMenu,
	DropdownMenuContent,
	DropdownMenuItem,
	DropdownMenuLabel,
	DropdownMenuSeparator,
	DropdownMenuTrigger,
} from 'components/ui/dropdown-menu';
import { Button } from 'components/ui/button';
import { MoreHorizontal } from 'lucide-react';
import { CellContext } from '@tanstack/react-table';
import { APIKey } from 'lib/BansAPI';
import { Sheet, SheetContent, SheetTitle } from 'components/ui/sheet';
import {
	AlertDialog,
	AlertDialogAction,
	AlertDialogCancel,
	AlertDialogContent,
	AlertDialogDescription,
	AlertDialogFooter,
	AlertDialogHeader,
	AlertDialogTitle,
} from 'components/ui/alert-dialog';
import { Input } from 'components/ui/input';
import { Label } from 'components/ui/label';
import {
	Select,
	SelectContent,
	SelectGroup,
	SelectItem,
	SelectTrigger,
	SelectValue,
} from 'components/ui/select';

type AlertData = {
	type: 'create';
} | {
	type: 'delete' | 'rollover';
	id: string;
} | {
	type: 'secret';
	id: string;
	key: string;
};

interface KeyEditState {
	name: string;
	permission: 'CREATE' | 'VIEW';
	id?: string;
}

function APIKeyActionsCell(
	setAlertData: (data: AlertData) => void,
	setEditState: (state: KeyEditState) => void,
) {
	return function Cell({ row }: CellContext<any, any>) {
		return (
			<DropdownMenu>
				<DropdownMenuTrigger asChild>
					<Button variant="ghost" className="h-8 w-8 p-0">
						<span className="sr-only">Open menu</span>
						<MoreHorizontal className="h-4 w-4" />
					</Button>
				</DropdownMenuTrigger>
				<DropdownMenuContent align="end">
					<DropdownMenuLabel>Actions</DropdownMenuLabel>
					<DropdownMenuItem
						className="cursor-pointer"
						onClick={() => setEditState({
							name: row.original.name,
							permission: row.original.permissions[0],
							id: row.original.id,
						})}
					>
						Edit
					</DropdownMenuItem>
					<DropdownMenuSeparator />
					<DropdownMenuLabel className="text-red-600 dark:text-red-500">Danger zone</DropdownMenuLabel>
					<DropdownMenuItem
						className="cursor-pointer"
						onClick={() => setAlertData({ type: 'rollover', id: row.original.id })}
					>
						Reset API key
					</DropdownMenuItem>
					<DropdownMenuItem
						className="cursor-pointer"
						onClick={() => setAlertData({ type: 'delete', id: row.original.id })}
					>
						Delete API key
					</DropdownMenuItem>
				</DropdownMenuContent>
			</DropdownMenu>
		);
	};
}

export default function BansAPIKeysPage() {
	const [data, setData] = useState<APIKey[] | null>(null);
	const [alertData, setAlertData] = useState<AlertData | null>(null);
	const [editState, setEditState] = useState<KeyEditState | null>(null);
	const [newName, setNewName] = useState<string | null>(null);
	const [newPermission, setNewPermission] = useState<'CREATE' | 'VIEW' | null>(null);

	useEffect(() => {
		(async () => {
			const res = await fetch('/api/bans/keys/list');
			setData(await res.json());
		})();
	}, []);

	if (!data) {
		return (
			<div className="p-8 grid place-items-center w-full">
				<Skeleton className="w-10/12 h-3/4" />
			</div>
		);
	}

	return (
		<Sheet
			open={!!editState?.id}
			onOpenChange={(open) => {
				if (!open) {
					setEditState(null);
					setNewName(null);
					setNewPermission(null);
				}
			}}
		>
			<div className="p-8 grid place-items-center w-full">
				<div className="container w-10/12 h-3/4 mx-auto">
					<h1 className="text-3xl font-medium">Manage your API keys</h1>
					<br />
					<Button onClick={() => {
						setNewPermission('CREATE');
						setAlertData({ type: 'create' });
					}}
					>
						Create new API key
					</Button>
					<DataTable
						columns={[
							{
								accessorKey: 'name',
								header: 'Name',
							},
							{
								accessorKey: 'permissions',
								header: 'Permissions',
							},
							{
								id: 'actions',
								cell: APIKeyActionsCell(setAlertData, setEditState),
							},
						]}
						data={data}
					/>
				</div>
			</div>

			<AlertDialog open={!!alertData}>
				<AlertDialogContent>
					<AlertDialogHeader>
						<AlertDialogTitle>
							{(() => {
								switch (alertData?.type) {
									case 'create':
										return 'Create a new API key';
									case 'secret':
										return 'API key';
									default:
										return 'Are you absolutely sure?';
								}
							})()}
						</AlertDialogTitle>

						<AlertDialogDescription>
							{(() => {
								switch (alertData?.type) {
									case 'create':
										return 'Create a new API key to use for the Bans API.';
									case 'delete':
										return 'This action cannot be undone. This will permanently delete the API key.';
									case 'rollover':
										return 'This action cannot be undone. This will immediately invalidate the old key.';
									case 'secret':
										return 'These credentials are only shown once, make sure to save them somewhere secure. The id is used as username for RabbitMQ real time updates.';
									default:
										return '';
								}
							})()}
						</AlertDialogDescription>
					</AlertDialogHeader>

					{alertData?.type === 'create' && (
						<div className="grid gap-4 py-4">
							<div className="grid grid-cols-4 items-center gap-4">
								<Label htmlFor="name" className="text-right">
									Name
								</Label>
								<Input
									id="name"
									value={newName ?? ''}
									onChange={(e) => setNewName(e.target.value)}
									className="col-span-3"
								/>
							</div>
							<div className="grid grid-cols-4 items-center gap-4">
								<Label className="text-right">
									Permission
								</Label>
								<Select
									value={newPermission ?? ''}
									onValueChange={(newValue) => setNewPermission(newValue as any)}
								>
									<SelectTrigger className="w-[180px]">
										<SelectValue placeholder="Select a permission" />
									</SelectTrigger>
									<SelectContent>
										<SelectGroup>
											<SelectItem value="CREATE">Create</SelectItem>
											<SelectItem value="VIEW">View</SelectItem>
										</SelectGroup>
									</SelectContent>
								</Select>
							</div>
						</div>
					)}

					{alertData?.type === 'secret' && (
						<div className="grid gap-4 py-4">
							<div className="grid grid-cols-4 items-center gap-4">
								<Label htmlFor="id" className="text-right">
									API key id
								</Label>
								<Input
									id="id"
									value={alertData.id}
									className="col-span-3"
									readOnly
								/>
							</div>
							<div className="grid grid-cols-4 items-center gap-4">
								<Label htmlFor="key" className="text-right">
									API key token
								</Label>
								<Input
									id="key"
									value={alertData.key}
									className="col-span-3"
									readOnly
								/>
							</div>
						</div>
					)}

					<AlertDialogFooter>
						{
							alertData?.type === 'secret'
								? (
									<AlertDialogCancel onClick={() => setAlertData(null)}>Close</AlertDialogCancel>
								) : (
									<>
										<AlertDialogCancel onClick={() => {
											setAlertData(null);
											setEditState(null);
											setNewName(null);
											setNewPermission(null);
										}}
										>
											Cancel
										</AlertDialogCancel>
										<AlertDialogAction
											onClick={async () => {
												try {
													if (alertData?.type === 'create') {
														const res = await fetch('/api/bans/keys/create', {
															method: 'POST',
															headers: {
																'Content-Type': 'application/json',
															},
															body: JSON.stringify({
																name: newName,
																permissions: [newPermission],
															}),
														});

														const body = await res.json();

														setAlertData({
															type: 'secret',
															id: body.id,
															key: body.key,
														});
														setData((oldData) => [
															...oldData!,
															{ name: body.name, id: body.id, permissions: [newPermission!] },
														]);
													} else if (alertData?.type === 'rollover') {
														const res = await fetch(`/api/bans/keys/${alertData.id}/rollover`, {
															method: 'POST',
														});

														const body = await res.json();

														setAlertData({
															type: 'secret',
															id: body.id,
															key: body.key,
														});
													} else if (alertData?.type === 'delete') {
														await fetch(`/api/bans/keys/${alertData.id}`, {
															method: 'DELETE',
														});

														setData((oldData) => {
															const index = oldData!.findIndex((key) => key.id === alertData.id);
															const copy = [...oldData!];
															copy.splice(index, 1);
															return copy;
														});

														setAlertData(null);
													}
												} catch (err) {
													// TODO: Handle errors better
													// eslint-disable-next-line no-console
													console.log(err);
												}

												setEditState(null);
												setNewName(null);
												setNewPermission(null);
											}}
										>
											Continue
										</AlertDialogAction>
									</>
								)
						}
					</AlertDialogFooter>
				</AlertDialogContent>
			</AlertDialog>

			<SheetContent>
				<SheetTitle>Edit API key</SheetTitle>
				<div className="grid gap-4 py-4">
					<div className="grid grid-cols-4 items-center gap-4">
						<Label htmlFor="name" className="text-right">
							Name
						</Label>
						<Input
							id="name"
							value={newName ?? editState?.name}
							onChange={(e) => setNewName(e.target.value)}
							className="col-span-3"
						/>
					</div>
					<div className="grid grid-cols-4 items-center gap-4">
						<Label className="text-right">
							Permission
						</Label>
						<Select
							value={newPermission ?? editState?.permission}
							onValueChange={(newValue) => setNewPermission(newValue as any)}
						>
							<SelectTrigger className="w-[180px]">
								<SelectValue placeholder="Select a permission" />
							</SelectTrigger>
							<SelectContent>
								<SelectGroup>
									<SelectItem value="CREATE">Create</SelectItem>
									<SelectItem value="VIEW">View</SelectItem>
								</SelectGroup>
							</SelectContent>
						</Select>
						<Button onClick={async () => {
							try {
								const res = await fetch(`/api/bans/keys/${editState!.id}`, {
									method: 'PATCH',
									headers: {
										'Content-Type': 'application/json',
									},
									body: JSON.stringify({
										name: newName ?? undefined,
										permissions: newPermission ? [newPermission] : undefined,
									}),
								});

								const body = await res.json();

								setData((oldData) => {
									const index = oldData!.findIndex((key) => key.id === editState!.id);
									const copy = [...oldData!];
									copy[index] = {
										id: body.id,
										name: body.name,
										permissions: body.permissions[0],
									};

									return copy;
								});
							} catch (err) {
								// TODO: Handle errors better
								// eslint-disable-next-line no-console
								console.error(err);
							}

							setEditState(null);
							setNewName(null);
							setNewPermission(null);
						}}
						>
							Save
						</Button>
					</div>
				</div>
			</SheetContent>
		</Sheet>
	);
}
