import {
	Card, CardContent, CardHeader, CardTitle,
} from 'components/ui/card';

export default function HomePage() {
	return (
		<div className="p-8 grid place-items-center w-full">
			<Card className="max-w-2xl">
				<CardHeader>
					<CardTitle>Suisei&apos;s Mic - Developer Portal</CardTitle>
				</CardHeader>
				<CardContent>
					<p>
						{/* eslint-disable-next-line max-len */}
						This portal is a work in progress and will get updated as time goes on, currently it only contains the bare minimum.
					</p>
				</CardContent>
			</Card>
		</div>
	);
}
