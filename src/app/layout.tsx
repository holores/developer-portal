import 'styles/globals.css';
import { Inter } from 'next/font/google';
import DarkModeProvider from 'components/providers/DarkModeProvider';
import ThemeSwitcher from 'components/ThemeSwitcher';

interface IProps {
	children: React.ReactNode;
}

const inter = Inter({ subsets: ['latin'] });

export const metadata = {
	title: "Suisei's Mic - Developer Portal",
	description: "The developer portal for Suisei's Mic.",
};

export default function RootLayout({ children }: IProps) {
	return (
		<html lang="en" suppressHydrationWarning>
			<head />
			<body className={inter.className}>
				<DarkModeProvider>
					{children}
					<ThemeSwitcher />
				</DarkModeProvider>
			</body>
		</html>
	);
}
