export interface APIKey {
	id: string;
	name: string;
	permissions: Array<'CREATE' | 'VIEW'>;
}

export interface Webhook {
	id: string;
	name: string;
	url: string;
	disabled: boolean;
	failures: number;
	subscriptions: string[];
}
