import DiscordProvider from 'next-auth/providers/discord';
import { NextAuthOptions } from 'next-auth';

declare module 'next-auth' {
	interface Session {
		user?: {
			id?: string | null;
			name?: string | null;
			email?: string | null;
			image?: string | null;
		}
	}
}

const acceptedGuilds = (process.env.ALLOWED_GUILDS as string).split(',').map((id) => id.trim());

const authOptions: NextAuthOptions = {
	providers: [
		DiscordProvider({
			clientId: process.env.DISCORD_CLIENT_ID as string,
			clientSecret: process.env.DISCORD_CLIENT_SECRET as string,
			authorization: 'https://discord.com/api/oauth2/authorize?scope=identify+guilds',
		}),
	],
	session: {
		// TODO: Move to session tokens
		strategy: 'jwt',
	},
	pages: {
		signIn: '/',
	},
	callbacks: {
		async signIn({ account }) {
			const res = await fetch('https://discord.com/api/users/@me/guilds', {
				headers: {
					authorization: `${account?.token_type} ${account?.access_token}`,
				},
			});

			const data: Array<{ id: string }> = await res.json();
			const guildIds = data.map((guild) => guild.id);

			// The length of the array is short enough that short circuiting does not make a difference
			return acceptedGuilds.map((id) => guildIds.includes(id)).includes(true);
		},
		async jwt({ token, profile }) {
			if (profile) {
				// @ts-expect-error id doesn't exist on Profile
				token.id = profile.id; // eslint-disable-line no-param-reassign
			}

			return token;
		},
		async session({ session, token }) {
			if (session.user) {
				// eslint-disable-next-line no-param-reassign
				session.user.id = token.id as string;
			}

			return session;
		},
	},
};

export default authOptions;
